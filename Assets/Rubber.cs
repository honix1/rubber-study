﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Rubber : MonoBehaviour {

    const int FRIENDS_COUNT = 6;
    struct IndexedVertex {
        public int index;
        public Vector3 vertex;
    }

    public Camera camera;

    public bool syncMesh = true;

    public float mass = 0.1f;
    public float damping = 0.85f;
    public float friendy = 0.1f;

    public float push = 0.2f;

    Mesh mesh;
    MeshCollider meshCollider;

    Vector3[] initialVertices;
    Vector3[] inertiaVertices;
    Vector3[] nextInertiaVertices;

    int[] friendsVertices;

    void Start() {
        mesh = GetComponent<MeshFilter>().mesh;
        meshCollider = GetComponent<MeshCollider>();

        mesh.MarkDynamic();

        initialVertices = mesh.vertices;
        inertiaVertices = new Vector3[mesh.vertices.Length];
        nextInertiaVertices = new Vector3[mesh.vertices.Length];

        IndexedVertex[] indexedVertices = new IndexedVertex[mesh.vertices.Length];
        for (int i = 0; i < initialVertices.Length; i++) {
            indexedVertices[i] = new IndexedVertex { index = i, vertex = initialVertices[i] };
        }

        friendsVertices = new int[initialVertices.Length * FRIENDS_COUNT];
        for (int i = 0; i < initialVertices.Length; i++) {
            int[] friends = indexedVertices
                .OrderBy(x => Vector3.Distance(initialVertices[i], x.vertex))
                .Skip(1) // itself
                .Take(FRIENDS_COUNT)
                .Select(x => x.index)
                .ToArray();
            friends.CopyTo(friendsVertices, i * FRIENDS_COUNT);
        }
    }
	
	void Update () {
        Vector3[] vertices   = mesh.vertices;
        Vector3[] normals    = mesh.normals;
        int[]     triangles  = mesh.triangles;

        int select = -1;
        if (Input.GetMouseButton(0)) {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 100);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit) && raycastHit.collider == meshCollider) {
                select = raycastHit.triangleIndex;
            }
        }

        if (select >= 0) {
            int shift = select * 3;
            for (var i = 0; i < 3; i++) {
                int v = triangles[shift + i];
                inertiaVertices[v] += normals[v] * -push;
            }
        }

        Array.Clear(nextInertiaVertices, 0, nextInertiaVertices.Length);

        for (int i = 0; i < inertiaVertices.Length; i++) {
            inertiaVertices[i] += Vector3.Lerp(vertices[i], initialVertices[i], mass) - vertices[i];
            inertiaVertices[i] *= damping;

            int friendsShift = i * FRIENDS_COUNT;
            for (int f = 0; f < FRIENDS_COUNT; f++) {
                int friend = friendsVertices[friendsShift + f];
                nextInertiaVertices[friend] += inertiaVertices[i] * friendy;
            }

            vertices[i] += inertiaVertices[i];
        }

        for (int i = 0; i < inertiaVertices.Length; i++) {
            inertiaVertices[i] += nextInertiaVertices[i];
        }

        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        if (syncMesh)
            meshCollider.sharedMesh = mesh;
    }
}
